import React from "react";
import {
  Navbar,
  Nav,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  Collapse,
  NavbarText,
    Container,Row,Col
} from "reactstrap";

class NavbarComp extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {};

  render() {
    return (
        <Container><Row><Col className="md-12"><Navbar
            color="light"
            expand="md"
            fixed="top"
            style={{
              boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
            }}
        >
          <NavbarBrand href="/">
            <h4 className="pt-3">Mande</h4>
          </NavbarBrand>
          <NavbarToggler onClick={function noRefCheck() {}} />
          <Collapse navbar>
            <Nav className="me-auto pt-2" navbar>
              <NavItem>
                <NavLink href="/transfer" style={{ color: "black" }}>
                  Transfer
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/scheduler" style={{ color: "black" }}>
                  Scheduler
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/users" style={{ color: "black" }}>
                  Users
                </NavLink>
              </NavItem>
            </Nav>
            <NavbarText style={{ cursor: "pointer" }}>Login</NavbarText>
            &nbsp;&nbsp;
            <NavLink href="/register">
              <NavbarText style={{ cursor: "pointer", color: "black" }}>
                Register
              </NavbarText>
            </NavLink>
          </Collapse>
        </Navbar>
        </Col></Row></Container>
    );
  }
}

export default NavbarComp;
