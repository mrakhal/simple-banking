import React from "react";
import {
  UncontrolledAccordion,
  AccordionItem,
  AccordionHeader,
  AccordionBody,
} from "reactstrap";

class AccordionComp extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {};
  render() {
    return (
      <div className="p-0 m-0">
        <UncontrolledAccordion defaultOpen={["1"]} stayOpen>
          <AccordionItem>
            <AccordionHeader targetId="1">
              Benefit Gold Card Bank Mandiri
            </AccordionHeader>
            <AccordionBody accordionId="1" style={{ fontSize: "13px" }}>
              <ul>
                <li>Biaya administrasi per bulan sebesar Rp. 4,500</li>
                <li>Gratis untuk upgrade dan Rp. 15,000 untuk downgrade</li>
                <li>
                  Untuk penarikan tunai di ATM, pemilik kartu bisa mengambil
                  hingga Rp. 10,000,000
                </li>
                <li>
                  Transfer ke sesama mandiri mempunyai limit Rp. 50,000,000 dan
                  ke rekening bank lain Rp. 10,000,000
                </li>
                <li>
                  Pembelian atau pembayaran tagihan di ATM tidak boleh lebih
                  dari Rp. 50,000,000 sedangkan menggunakan
                </li>
                <li>EDC hanya bisa maksimal Rp. 2,000,000</li>
                <li>
                  Jika belanja di merchant atau EDC, maka hanya bisa maksimal
                  Rp. 50,000,000
                </li>
              </ul>
            </AccordionBody>
          </AccordionItem>
        </UncontrolledAccordion>
      </div>
    );
  }
}

export default AccordionComp;
