import React from "react";
import { Carousel } from "react-responsive-carousel";
import { Container } from "reactstrap";

class CarouselComp extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {};
  render() {
    return (
      <Container fluid className="p-0">
        <Carousel
          showArrows={true}
          showIndicators={false}
          showStatus={false}
          showThumbs={false}
          interval={5000}
          autoPlay={true}
          infiniteLoop={true}
        >
          <div>
            <img
              src="https://assets.promediateknologi.com/crop/0x0:0x0/x/photo/2022/05/01/607117687.jpg"
              style={{ objectFit: "cover" }}
            />
          </div>
          <div>
            <img
              src="https://miro.medium.com/max/1400/1*FGxHKmj3k7yPaJkv3C1-ow.png"
              style={{ objectFit: "cover" }}
            />
          </div>
        </Carousel>
      </Container>
    );
  }
}

export default CarouselComp;
