import React from "react";

class FooterComp extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {};
  render() {
    return (
      <>
        <footer className="bg-light text-center text-lg-start mt-5">
          <div
            className="text-center p-4"
            style={{ backgroundColor: "#2469A5" }}
          >
            <a
              href="https://mdbootstrap.com/"
              style={{
                color: "white",
                textDecoration: "none",
                fontWeight: 700,
              }}
            >
              Bank Mande
            </a>
            <p style={{ color: "white", fontSize: "13px" }}>
              Terdepan, Terpecaya, Terbaik
            </p>
          </div>
        </footer>
      </>
    );
  }
}

export default FooterComp;
