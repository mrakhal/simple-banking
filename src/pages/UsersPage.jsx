import React from "react";
import { Container, Row, Col, Card, Table } from "reactstrap";
import axios from "axios";

class UsersPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    this.getUsers();
  }

  getUsers() {
    axios
      .get("http://localhost:8080/api/user")
      .then((res) => {
        this.setState({ users: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    return (
      <>
        <Container className="mt-5" fluid style={{height:"100vh"}}>
          <Row>
            <Col md="12 mt-5">
              <Container>
                <Row>
                  <Col md="12">
                    <div>
                      <h5>Dashboard Users</h5>
                      <div
                        style={{
                          border: "1px solid #A5B3C5",
                          padding: "10px",
                          borderRadius: "10px",
                        }}
                      >
                        <Container>
                          <Row>
                            <Col md="4">
                              <Card
                                className="p-2"
                                style={{ borderRadius: "10px" }}
                              >
                                <div className="d-flex justify-content-between">
                                  <p style={{ fontSize: "13px" }}>
                                    Total Users
                                  </p>
                                  <p
                                    style={{
                                      color: "#A5B3C5",
                                      fontSize: "13px",
                                    }}
                                  >
                                    Registered
                                  </p>
                                </div>

                                <div>
                                  <h2>{this.state.users.length}</h2>
                                </div>
                              </Card>
                            </Col>
                            <Col md="8">{/* WAIT FOR UI */}</Col>
                          </Row>
                        </Container>
                      </div>
                    </div>
                  </Col>
                  <Col md="4"></Col>
                  <Col md="4"></Col>

                  {/* TABLE LIST */}

                  <Col md="12 mt-4">
                    <Table striped style={{ width: "100%" }}>
                      <thead>
                        <tr style={{ fontSize: "14px", textAlign: "center" }}>
                          <th>No</th>
                          <th>Account Number</th>
                          <th>Name</th>
                          <th>Nik</th>
                          <th>Daily Transaction</th>
                          <th>Membership</th>
                          <th>Maximum Transfer Daily</th>
                        </tr>
                      </thead>
                      <tbody style={{ textAlign: "center" }}>
                        {this.state.users.map((item, idx) => {
                          return (
                            <tr style={{ fontSize: "13px" }}>
                              <th scope="row">{idx + 1}</th>
                              <td>{item.id}</td>
                              <td>{item.name}</td>
                              <td>{item.nik}</td>
                              <td>{item.dailyTransaction}</td>
                              <td>{item.card.membership}</td>
                              <td>{item.card.maximumTransferDaily}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default UsersPage;
