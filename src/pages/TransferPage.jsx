import React from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardTitle,
  CardText,
  CardBody,
  Input,
  Label,
} from "reactstrap";
import axios from "axios";

class TransferPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cronExpression: "",
      action: "",
      id: "",
      amount: "",
    };
    this.onChangeAmount = this.onChangeAmount.bind(this);
    this.onChangeId = this.onChangeId.bind(this);
  }

  onBtnSubmit = () => {
    let id = this.state.id;
    let amount = this.state.amount;
    axios
      .post(`http://localhost:8080/api/transfer`, {
        id: id,
        amount: amount,
      })
      .then((res) => {
        this.setState({ id: "", amount: "" });
        alert("Successfully Transfered");
      })
      .catch((err) => {
        alert("Your maximum transfer reach the limit transfer!");
        this.setState({ id: "", amount: "" });
      });
  };

  onChangeId(e) {
    console.log(e.target.value);
    this.setState({
      id: e.currentTarget.value,
    });
  }

  onChangeAmount(e) {
    console.log(e.target.value);
    this.setState({
      amount: e.currentTarget.value,
    });
  }

  render() {
    return (
      <Container className="mt-5">
        <Row>
          <Col md="4 mt-5">
            <h2 style={{ fontWeight: "700" }}>
              Platforms help your money to be better
            </h2>
            <p style={{ fontSize: "14px" }}>
              You no longer need to go home or go to the bank to do this. Only
              through this platform, all financial activities can be completed.
            </p>
            <div className="d-flex justify-space-evenly align-items-center">
              <Button
                style={{
                  backgroundColor: "#2469A5",
                  border: "none",
                  borderRadius: "20px",
                }}
              >
                Get Started
              </Button>
              <Button
                style={{
                  backgroundColor: "#fff",
                  color: "black",
                  border: "1px solid black",
                  borderRadius: "20px",
                  marginLeft: "5px",
                }}
              >
                Learn More
              </Button>
            </div>
          </Col>
          <Col md="8 mt-4">
            <img
              src="https://bankmandiri.co.id/documents/20143/42263731/ag-restrukturisasi-3%402x.png/54063bed-8546-2f0e-8bd9-78240ff88044?t=1587009740906"
              width="100%"
              height="90%"
            />
          </Col>

          <Col md="12">
            <h5 style={{ textAlign: "center" }}>Transfer Now</h5>
            <Card style={{ boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px" }}>
              <Container>
                <Row>
                  <Col md="6">
                    {" "}
                    <CardBody>
                      <Label for="Account Number" style={{ fontSize: "14px" }}>
                        Account Number
                      </Label>
                      <Input
                        type="text"
                        placeholder="Please input your Account Number"
                        onChange={this.onChangeId}
                        value={this.state.id}
                      />
                    </CardBody>{" "}
                  </Col>
                  <Col md="4">
                    {" "}
                    <CardBody>
                      <Label for="Account Number" style={{ fontSize: "14px" }}>
                        Amount
                      </Label>
                      <Input
                        type="number"
                        placeholder="Please input amount"
                        onChange={this.onChangeAmount}
                        value={this.state.amount}
                      />
                    </CardBody>{" "}
                  </Col>
                  <Col md="2 mt-4 pt-1">
                    {" "}
                    <CardBody>
                      <Button color="primary" onClick={this.onBtnSubmit}>
                        Transfer
                      </Button>
                    </CardBody>{" "}
                  </Col>
                </Row>
              </Container>
            </Card>
          </Col>

          <Col md="12 mt-5">
            <h5 style={{ textAlign: "center" }}>
              App features that help manage money more organized
            </h5>
          </Col>

          <Col md="12 mt-4">
            <Container>
              <Row>
                <Col md="4">
                  <Card
                    style={{
                      border: "none",
                      borderRadius: "10px",
                      boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
                      padding: "10px",
                      overflow: "hidden",
                      height: "60vh",
                    }}
                  >
                    <CardTitle
                      style={{
                        textAlign: "center",
                        color: "#2469A5",
                        fontWeight: "700",
                      }}
                    >
                      Facilitate Financial Service
                    </CardTitle>
                    <CardText
                      style={{ textAlign: "justify", fontSize: "13px" }}
                    >
                      Consequat curae neque amet aucor, sed fames sem tristique,
                      nulliam nisi macenes.
                    </CardText>
                    <CardBody style={{ position: "absolute", bottom: -120 }}>
                      <img
                        src="https://www.pngplay.com/wp-content/uploads/12/Smartphone-PNG-Background.png"
                        width="100%"
                      />
                    </CardBody>
                  </Card>
                </Col>
                <Col md="4">
                  <Card
                    style={{
                      border: "none",
                      borderRadius: "10px",
                      boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
                      padding: "10px",
                      overflow: "hidden",
                      height: "60vh",
                    }}
                  >
                    <CardTitle
                      style={{
                        textAlign: "center",
                        color: "#2469A5",
                        fontWeight: "700",
                      }}
                    >
                      Facilitate Financial Service
                    </CardTitle>
                    <CardText
                      style={{ textAlign: "justify", fontSize: "13px" }}
                    >
                      Consequat curae neque amet aucor, sed fames sem tristique,
                      nulliam nisi macenes.
                    </CardText>
                    <CardBody style={{ position: "absolute", bottom: -120 }}>
                      <img
                        src="https://www.pngplay.com/wp-content/uploads/12/Smartphone-PNG-Background.png"
                        width="100%"
                      />
                    </CardBody>
                  </Card>
                </Col>
                <Col md="4">
                  <Card
                    style={{
                      border: "none",
                      borderRadius: "10px",
                      boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
                      padding: "10px",
                      overflow: "hidden",
                      height: "60vh",
                    }}
                  >
                    <CardTitle
                      style={{
                        textAlign: "center",
                        color: "#2469A5",
                        fontWeight: "700",
                      }}
                    >
                      Facilitate Financial Service
                    </CardTitle>
                    <CardText
                      style={{ textAlign: "justify", fontSize: "13px" }}
                    >
                      Consequat curae neque amet aucor, sed fames sem tristique,
                      nulliam nisi macenes.
                    </CardText>
                    <CardBody style={{ position: "absolute", bottom: -120 }}>
                      <img
                        src="https://www.pngplay.com/wp-content/uploads/12/Smartphone-PNG-Background.png"
                        width="100%"
                      />
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default TransferPage;
