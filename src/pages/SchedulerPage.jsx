import axios from "axios";
import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
    Label,
  Input,
  FormGroup
} from "reactstrap";

class SchedulerPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schedule: [],
      cronExpression: "",
      action: "",
      isOpenModal: false,
      jobId: [],
      second:"0",
      minute:"0",
      hour:"0",
      day:"*",
      month:"*",
      dayOfWeek:"?",
      year:"*"
    };
    this.handleChangeInputSecond = this.handleChangeInputSecond.bind(this);
    this.handleChangeInputMinute = this.handleChangeInputMinute.bind(this);
    this.handleChangeInputHour = this.handleChangeInputHour.bind(this);
    this.handleChangeInputDay = this.handleChangeInputDay.bind(this);
    this.handleChangeInputMonth = this.handleChangeInputMonth.bind(this);
    this.handleChangeInputDayOfWeek = this.handleChangeInputDayOfWeek.bind(this);
    this.onChangeAction = this.onChangeAction.bind(this);
  }

  timely=[...Array(61).keys()].slice(1,61)
  hourly = [...Array(25).keys()].slice(1,32)
  day = [...Array(32).keys()].slice(1.32)
  dayOfWeek = [...Array(8).keys()].slice(1.8)
  month = [...Array(13).keys()].slice(1.13)
  years = [new Date().getFullYear(),new Date().getFullYear()+1,new Date().getFullYear()+2,new Date().getFullYear()+3,new Date().getFullYear()+4,new Date().getFullYear()+5,new Date().getFullYear()+6,new Date().getFullYear()+7,new Date().getFullYear()+8]

  componentDidMount() {
    this.getSchedule();
  }

  onBtnDelete(jobId) {
    axios
      .delete(`http://localhost:8080/schedule/remove/${jobId}`)
      .then((res) => {
        alert("Job Id Successfully Deleted");
        this.getSchedule();
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  getSchedule() {
    axios
      .get("http://localhost:8080/schedule/taskdef")
      .then((res) => {
        console.log("isi res",res)
        this.setState((this.state.schedule = res.data));
      })
      .catch((e) => {
        console.log(e.response);
      });
  }

  handleChangeInputSecond(event) {
    this.setState({ second: event.target.value });
  }

  handleChangeInputMinute(event) {
    this.setState({ minute: event.target.value });
    console.log("minute",event)
  }

  handleChangeInputHour(event) {
    this.setState({ hour: event.target.value });
    console.log("hour",event)
  }

  handleChangeInputDay(event) {
    this.setState({ day: event.target.value });
  }

  handleChangeInputMonth(event) {
    this.setState({ month: event.target.value });
  }


  handleChangeInputDayOfWeek(event) {
    this.setState({ dayOfWeek: event.target.value });
  }

  // handleChangeInputYear(event) {
  //   this.setState({ year: event.target.value });
  // }

  onBtnSubmit = () => {
    var time = new Date();
    var month = time.getMonth() + 1;
    let cronExpression = this.state.second +" "+ this.state.minute +" "+ this.state.hour +" "+ this.state.day +" "+ this.state.month +" "+this.state.dayOfWeek;
    let action = this.state.action;

    console.log(cronExpression+" ini cron Expression")
    axios
      .post(`http://localhost:8080/schedule/taskdef`, {
        cronExpression: cronExpression,
        action: action,
      })
      .then((res) => {
        alert(
          `Cron Expression successfully created! Running At ` +
            time.getFullYear() +
            "-" +
            month +
            "-" +
            time.getDate() +
            cronExpression.slice(5, 7) +
            ":" +
            cronExpression.slice(2, 4)
        );
        this.setState({ cronExpression: "", action: "" });
        this.getSchedule();
      })
      .catch((err) => {
        alert("Input salah!");
      });
  };

  onBtnEdit = () => {
    let id = this.state.jobId.id;
    let cronExpression = this.state.second +" "+ this.state.minute +" "+ this.state.hour +" "+ this.state.day +" "+ this.state.month +" "+this.state.dayOfWeek;
    let action = this.state.action;
    console.log(id + cronExpression + action);
    axios
      .put(`http://localhost:8080/schedule/taskdef/edit`, {
        id: id,
        cronExpression: cronExpression,
        action: action,
      })
      .then((res) => {
        this.setState({ cronExpression: "", action: "", isOpenModal: false });
        alert("Successfully edit with Job Id: " + id);
        this.getSchedule();
      })
      .catch((err) => {
        alert("Input salah!");
      });
  };

  onChangeAction(e) {
    this.setState({
      action: e.currentTarget.value,
    });
  }

  render() {
    return (
      <Container className="mt-5" fluid style={{height:"100vh"}}>
        <Row>
          <Col md="12 mt-5">
            <Container>
              <Row>
                <Col md="12">
                  {/* MODAL */}
                  <Modal isOpen={this.state.isOpenModal}>
                    <ModalHeader>
                      <span style={{ fontSize: "17px" }}>Edit Schedule</span>
                    </ModalHeader>
                    <ModalBody>
                      <Container>
                        <Row>
                          <Col md="4">
                            <FormGroup>
                              <Label style={{
                                fontSize: "12px",
                                color: "gray",
                              }} for="exampleSelect">Second</Label>
                              <Input type="select" name="select" id="exampleSelect" value={this.state.second} onChange={(e) => this.handleChangeInputSecond(e)}>
                                <option value="*">0</option>
                                {this.timely.map((item,idx)=>{
                                  return(<option value={item}>{item}</option>)
                                })}
                              </Input>
                            </FormGroup>
                          </Col>

                          <Col md="4">
                            <FormGroup>
                              <Label style={{
                                fontSize: "12px",
                                color: "gray",
                              }} for="exampleSelect">Minutes</Label>
                              <Input type="select" name="select" id="exampleSelect" value={this.state.minute} onChange={this.handleChangeInputMinute}>
                                <option value="*">0</option>
                                {this.timely.map((item,idx)=>{
                                  return(<option value={item}>{item}</option>)
                                })}
                              </Input>
                            </FormGroup>
                          </Col>

                          <Col md="4">
                            <FormGroup>
                              <Label style={{
                                fontSize: "12px",
                                color: "gray",
                              }} for="exampleSelect">Hour</Label>
                              <Input type="select" name="select" id="exampleSelect" value={this.state.hour} onChange={this.handleChangeInputHour}>
                                <option value="*">0</option>
                                {this.hourly.map((item,idx)=>{
                                  return(<option value={item}>{item}</option>)
                                })}
                              </Input>
                            </FormGroup>
                          </Col>

                          <Col md="4">
                            <FormGroup>
                              <Label style={{
                                fontSize: "12px",
                                color: "gray",
                              }} for="exampleSelect">Date Of Month</Label>
                              <Input type="select" name="select" id="exampleSelect" value={this.state.day} onChange={this.handleChangeInputDay}>
                                <option value="*">Every Day</option>
                                {this.day.map((item,idx)=>{
                                  return(<option value={item}>{item}</option>)
                                })}
                              </Input>
                            </FormGroup>
                          </Col>

                          <Col md="4">
                            <FormGroup>
                              <Label style={{
                                fontSize: "12px",
                                color: "gray",
                              }} for="exampleSelect">Month</Label>
                              <Input type="select" name="select" id="exampleSelect" value={this.state.mon} onChange={this.handleChangeInputMonth}>
                                <option value="*">Every Month</option>
                                {this.month.map((item,idx)=>{
                                  return(<option value={item}>{item}</option>)
                                })}
                              </Input>
                            </FormGroup>
                          </Col>

                          <Col md="4">
                            <FormGroup>
                              <Label style={{
                                fontSize: "12px",
                                color: "gray",
                              }} for="exampleSelect">Day Of Week</Label>
                              <Input type="select" name="select" id="exampleSelect" onChange={this.handleChangeInputDayOfWeek}>
                                <option value="*">Every Day</option>
                                {this.dayOfWeek.map((item,idx)=>{
                                  return(<option value={item}>{item}</option>)
                                })}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col md="12">
                            <label
                              for="title"
                              style={{
                                fontSize: "12px",
                                color: "gray",
                              }}
                            >
                              Action Label
                            </label>
                            <div className="d-flex">
                              <input
                                type="text"
                                placeholder={this.state.jobId.action}
                                style={{
                                  border: "1px solid #C8C8C8",
                                  width: "100%",
                                  borderRadius: "5px",
                                }}
                                onChange={this.onChangeAction}
                                value={this.state.action}
                              />
                            </div>
                          </Col>
                        </Row>
                      </Container>
                    </ModalBody>
                    <ModalFooter>
                      <Button
                        color="primary"
                        onClick={() => {
                          this.onBtnEdit();
                        }}
                      >
                        Submit
                      </Button>{" "}
                      <Button
                        color="danger"
                        onClick={() => {
                          this.setState({ isOpenModal: false });
                        }}
                      >
                        Cancel
                      </Button>
                    </ModalFooter>
                  </Modal>

                  {/* DASHBOARD SCHEDULER */}
                </Col>
                <Col md="12">
                  <div>
                    <h5>Dashboard Scheduler</h5>
                    <div
                      style={{
                        border: "1px solid #A5B3C5",
                        padding: "10px",
                        borderRadius: "10px",
                      }}
                    >
                      <Container>
                        <Row>
                          <Col md="4">
                            <Card
                              className="p-2"
                              style={{ borderRadius: "10px" }}
                            >
                              <div className="d-flex justify-content-between">
                                <p style={{ fontSize: "13px" }}>
                                  Scheduler Total
                                </p>
                                <p
                                  style={{ color: "#A5B3C5", fontSize: "13px" }}
                                >
                                  Daily Running
                                </p>
                              </div>

                              <div>
                                <h2>{this.state.schedule.length}</h2>
                              </div>
                            </Card>
                          </Col>
                          <Col md="8">
                            <Card
                              className="p-2"
                              style={{ borderRadius: "10px" }}
                            >
                              <div className="d-flex justify-content-between">
                                <p style={{ fontSize: "13px" }}>
                                  Add Scheduler
                                </p>
                                <p
                                  style={{ color: "#A5B3C5", fontSize: "13px" }}
                                >
                                  Scheduler
                                </p>
                              </div>

                              <Container>
                                <Row>
                                  <Col md="4">
                                    <FormGroup>
                                      <Label style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }} for="exampleSelect">Second</Label>
                                      <Input type="select" name="select" id="exampleSelect" onChange={(e) => this.handleChangeInputSecond(e)}>
                                        <option value="*">0</option>
                                        {this.timely.map((item,idx)=>{
                                          return(<option value={item}>{item}</option>)
                                        })}
                                      </Input>
                                    </FormGroup>
                                  </Col>

                                  <Col md="4">
                                    <FormGroup>
                                      <Label style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }} for="exampleSelect">Minutes</Label>
                                      <Input type="select" name="select" id="exampleSelect" onChange={this.handleChangeInputMinute}>
                                        <option value="*">0</option>
                                        {this.timely.map((item,idx)=>{
                                          return(<option value={item}>{item}</option>)
                                        })}
                                      </Input>
                                    </FormGroup>
                                  </Col>

                                  <Col md="4">
                                    <FormGroup>
                                      <Label style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }} for="exampleSelect">Hour</Label>
                                      <Input type="select" name="select" id="exampleSelect" onChange={this.handleChangeInputHour}>
                                        <option value="*">0</option>
                                        {this.hourly.map((item,idx)=>{
                                          return(<option value={item}>{item}</option>)
                                        })}
                                      </Input>
                                    </FormGroup>
                                  </Col>

                                  <Col md="4">
                                    <FormGroup>
                                      <Label style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }} for="exampleSelect">Date Of Month</Label>
                                      <Input type="select" name="select" id="exampleSelect" onChange={this.handleChangeInputDay}>
                                        <option value="*">Every Day</option>
                                        {this.day.map((item,idx)=>{
                                          return(<option value={item}>{item}</option>)
                                        })}
                                      </Input>
                                    </FormGroup>
                                  </Col>

                                  <Col md="4">
                                    <FormGroup>
                                      <Label style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }} for="exampleSelect">Month</Label>
                                      <Input type="select" name="select" id="exampleSelect" onChange={this.handleChangeInputMonth}>
                                        <option value="*">Every Month</option>
                                        {this.month.map((item,idx)=>{
                                          return(<option value={item}>{item}</option>)
                                        })}
                                      </Input>
                                    </FormGroup>
                                  </Col>

                                  <Col md="4">
                                    <FormGroup>
                                      <Label style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }} for="exampleSelect">Day Of Week</Label>
                                      <Input type="select" name="select" id="exampleSelect" onChange={this.handleChangeInputDayOfWeek}>
                                        <option value="*">Every Day</option>
                                        {this.dayOfWeek.map((item,idx)=>{
                                          return(<option value={item}>{item}</option>)
                                        })}
                                      </Input>
                                    </FormGroup>
                                  </Col>

                                  <Col md="12">
                                    <label
                                      for="title"
                                      style={{
                                        fontSize: "12px",
                                        color: "gray",
                                      }}
                                    >
                                      Action Label
                                    </label>
                                    <div className="d-flex pt-2">
                                      <input
                                        type="text"
                                        style={{
                                          border: "1px solid #C8C8C8",
                                          borderRadius: "5px",
                                          height:"4.5vh",
                                          width:"60%"
                                        }}
                                        onChange={this.onChangeAction}
                                        value={this.state.action}
                                      />
                                      <Button
                                          color="primary"
                                          style={{
                                            marginLeft:"6px",
                                            borderRadius: "8px",
                                            width:"40%"
                                          }}
                                          onClick={() => {
                                            this.onBtnSubmit();
                                          }}
                                      >Create
                                      </Button>
                                    </div>
                                  </Col>
                                </Row>
                              </Container>
                            </Card>
                          </Col>
                        </Row>
                      </Container>
                    </div>
                  </div>
                </Col>
                <Col md="4"></Col>
                <Col md="4"></Col>

                {/* TABLE LIST */}

                <Col md="12 mt-4">
                  <Table striped style={{ width: "100%" }}>
                    <thead>
                      <tr style={{ fontSize: "14px", textAlign: "center" }}>
                        <th>No</th>
                        <th>Job Id</th>
                        <th>Action</th>
                        <th>Cron</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody style={{ textAlign: "center" }}>
                      {this.state.schedule.map((item, idx) => {
                        var time = new Date();
                        var month = new Date().getMonth() + 1;
                        return (
                          <tr style={{ fontSize: "13px" }}>
                            <th scope="row">{idx + 1}</th>
                            <td>{item.id}</td>
                            <td>{item.action}</td>
                            <td>{item.cronExpression}</td>
                            <td>{item.createdAt.slice(0, 10)}</td>
                            <td>
                              {item.updateAt == null
                                ? "-"
                                : item.updatedAt.slice(0, 10)}
                            </td>
                            <td>
                              <div className="d-flex">
                                <Button
                                  color="danger"
                                  onClick={() => {
                                    this.onBtnDelete(item.id);
                                  }}
                                >
                                  Delete
                                </Button>
                                <Button
                                  style={{ marginLeft: "5px" }}
                                  color="primary"
                                  onClick={() => {
                                    this.setState(
                                      {
                                        jobId: item,
                                        isOpenModal: true,
                                      },
                                      () => {
                                        console.log(this.state.jobId);
                                      }
                                    );
                                  }}
                                >
                                  Edit
                                </Button>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default SchedulerPage;
