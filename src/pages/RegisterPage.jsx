import React from "react";
import { Container, Row, Col, Input, Button, ButtonGroup } from "reactstrap";
import axios from "axios";
import AccordionComp from "../components/AccordionComp";
import CarouselComp from "../components/CarouselComp";

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      name: "",
      card: "",
      nik: "",
      images: [
        {
          images:
            "https://www.bankmandiri.co.id/documents/20143/43982274/ag-landingpage-5.png/a4a5d947-d893-b4f8-7d29-30b1f864c916?t=1611125448472",
        },
        {
          images:
            "https://www.bankmandiri.co.id/documents/20143/43982274/ag-landingpage-6.png/dcd3adf8-6414-fbd5-e733-e47e6478b7e7?t=1611125448552",
        },
        {
          images:
            "https://www.bankmandiri.co.id/documents/20143/43982274/ag-landingpage-7.png/e01a9bfd-d3d0-9bf5-3f4d-39e8d5898a73?t=1611125448631https://www.bankmandiri.co.id/documents/20143/43982274/ag-landingpage-7.png/e01a9bfd-d3d0-9bf5-3f4d-39e8d5898a73?t=1611125448631",
        },
      ],
      membership: "",
    };
    this.onSiteChanged = this.onSiteChanged.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeNik = this.onChangeNik.bind(this);
  }

  componentDidMount() {
    this.getCard();
  }

  onBtnSubmit = () => {
    var cardId = 0;
    this.state.cards.map((item, idx) => {
      if (item.membership == this.state.membership) {
        cardId = item.id;
      }
    });
    let name = this.state.name;
    let nik = this.state.nik;
    let card = {
      id: cardId,
    };

    console.log(name, nik, card);
    axios
      .post(`http://localhost:8080/api/user`, {
        name: name,
        nik: nik,
        card: card,
      })
      .then((res) => {
        this.getCard();
        alert("Success Registration");
        this.setState({ name: "", card: "", nik: "" });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getCard() {
    axios
      .get("http://localhost:8080/api/card")
      .then((res) => {
        this.setState((this.state.cards = res.data));
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onSiteChanged(e) {
    console.log(e.target.value);
    this.setState({
      membership: e.currentTarget.value,
    });
  }

  onChangeName(e) {
    console.log(e.target.value);
    this.setState({
      name: e.currentTarget.value,
    });
  }
  onChangeNik(e) {
    console.log(e.target.value);
    this.setState({
      nik: e.currentTarget.value,
    });
  }

  render() {
    return (
      <Container fluid className="mt-5 p-0 m-0">
        <Row>
          <Col md="12 mt-5 p-0 m-0">
            <Container>
              <Row>
                <Col md="6 m-0 p-0">
                  <p style={{ fontSize: "14px" }}>
                    Welcome to mandirian tangguh, please put your login
                    credentials below to start using the app.
                  </p>
                  <Container>
                    <Row>
                      <Col md="4">
                        <p style={{ fontSize: "13px" }}>Name</p>
                      </Col>
                      <Col md="8">
                        <Input
                          type="text"
                          onChange={this.onChangeName}
                          value={this.state.name}
                        />
                      </Col>
                    </Row>
                  </Container>
                  <Container className="mt-3">
                    <Row>
                      <Col md="4">
                        <p style={{ fontSize: "13px" }}>NIK</p>
                      </Col>
                      <Col md="8">
                        <Input
                          type="text"
                          onChange={this.onChangeNik}
                          value={this.state.nik}
                        />
                      </Col>
                    </Row>
                  </Container>
                  <Container>
                    <Row>
                      <Col md="12 mt-2">
                        <p style={{ fontSize: "14px" }}>Select Card Type : </p>
                      </Col>
                      <Col md="12">
                        <ButtonGroup>
                          <Container>
                            <Row>
                              {this.state.cards.map((item, idx) => {
                                return (
                                  <Col md="6">
                                    <Col md="12">
                                      <input
                                        type="radio"
                                        id="html"
                                        name="fav_language"
                                        value={item.membership}
                                        onChange={this.onSiteChanged}
                                      />
                                       {" "}
                                      <label
                                        for="html"
                                        style={{
                                          textTransform: "capitalize",
                                          fontSize: "14px",
                                          fontWeight: "600",
                                        }}
                                      >
                                        {item.membership}
                                      </label>
                                    </Col>
                                    <img
                                      src={this.state.images[idx].images}
                                      width="100%"
                                    />
                                  </Col>
                                );
                              })}
                            </Row>
                          </Container>
                        </ButtonGroup>
                      </Col>
                    </Row>
                  </Container>

                  <Container>
                    <Row>
                      <Col md="12 mt-5">
                        <Button
                          style={{ width: "100%" }}
                          color="primary"
                          onClick={() => {
                            this.onBtnSubmit();
                          }}
                        >
                          Register
                        </Button>
                      </Col>
                    </Row>
                  </Container>

                  {/* <Container>
                    <Row>
                      <Col md="12 mt-2">
                        <div>
                          <p
                            style={{
                              textAlign: "right",
                              fontSize: "13px",
                              color: "#2469A5",
                            }}
                          >
                            Forgot Password?
                          </p>
                        </div>
                      </Col>
                    </Row>
                  </Container> */}
                  <Container>
                    <Row>
                      <Col md="12"></Col>
                    </Row>
                  </Container>
                </Col>

                <Col md="6 m-0 p-0">
                  <CarouselComp />

                  {this.state.membership == "gold" ? (
                    <Container className="mt-3">
                      <AccordionComp />
                    </Container>
                  ) : (
                    <></>
                  )}
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default RegisterPage;
