import React from "react";
import {
  Col,
  Container,
  Row,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Button,
} from "reactstrap";
import CarouselComp from "../components/CarouselComp";

class LandingPage extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {};
  render() {
    return (
      <>
        <Col md="12 mt-5">
          <CarouselComp />
        </Col>
        <Col md="12" style={{ backgroundColor: "#2469A5", color: "white" }}>
          <Container>
            <Row
              className="d-flex justify-content-center align-items-center p-5"
              style={{ fontSize: "10px" }}
            >
              <Col md="3">
                <div className="d-flex justify-content-center align-items-center">
                  <img
                    src="https://cdn-icons-png.flaticon.com/512/4289/4289322.png"
                    width={60}
                    height={60}
                  />
                  <p className="pt-3">Jelajahi keunggulan produk mande kartu kredit</p>
                </div>
              </Col>
              <Col md="3">
                <div className="d-flex justify-content-center align-items-center">
                  <img
                    src="https://cdn-icons.flaticon.com/png/512/5628/premium/5628131.png?token=exp=1652149234~hmac=ef4a0a2afb2736e5a82c3214ee6afb1b"
                    width={60}
                    height={60}
                  />
                  <p className="pt-3">Buka tabungan mande online sekarang juga!</p>
                </div>
              </Col>
              <Col md="3">
                <div className="d-flex justify-content-center align-items-center">
                  <img
                    src="https://cdn-icons.flaticon.com/png/512/3137/premium/3137807.png?token=exp=1652149380~hmac=2e0501b7faf5ab15b1cfe041487d94df"
                    width={60}
                    height={60}
                  />
                  <p className="pt-3">Jelajahi keunggulan produk mande kartu kredit</p>
                </div>
              </Col>
            </Row>
          </Container>
        </Col>
        <Col md="12 mt-5">
          <Container>
            <Row>
              <Col md="4">
                <Card style={{ border: "none " }}>
                  <CardImg
                    height="200vh"
                    src="https://cdn.pixabay.com/photo/2018/06/26/13/58/news-3499448_1280.jpg"
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle style={{ color: "#2469A5", fontWeight: "700" }}>
                      Pengumuman Kenaikan PPN Pada Tahun 2022
                    </CardTitle>
                    <CardSubtitle>01 Apr 2022</CardSubtitle>
                    <CardText style={{ color: "#606369", fontSize: "13px" }}>
                      Mulai tanggal 1 April 2022 berlaku perubahan Pajak
                      Pertambahan Nilai pada produk Mande.
                    </CardText>
                    <Button color="primary">See More</Button>
                  </CardBody>
                </Card>
              </Col>
              <Col md="4">
                <Card style={{ border: "none " }}>
                  <CardImg
                    height="200vh"
                    src="https://cdn.pixabay.com/photo/2018/06/26/13/58/news-3499448_1280.jpg"
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle
                      style={{
                        color: "#2469A5",
                        fontWeight: "700",
                        textAlign: "justify",
                      }}
                    >
                      Batas Nilai Saldo Tersimpan Uang Elektronik Naik Menjadi
                      Rp20 juta
                    </CardTitle>
                    <CardSubtitle>21 Apr 2022</CardSubtitle>
                    <CardText
                      style={{
                        color: "#606369",
                        fontSize: "13px",
                        textAlign: "justify",
                      }}
                    >
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </CardText>
                    <Button color="primary">See More</Button>
                  </CardBody>
                </Card>
              </Col>
              <Col md="4">
                <Card style={{ border: "none " }}>
                  <CardImg
                    height="200vh"
                    src="https://cdn.pixabay.com/photo/2018/06/26/13/58/news-3499448_1280.jpg"
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle style={{ color: "#2469A5", fontWeight: "700" }}>
                      Bank Mande Masuk Daftar Bank Terbaik Di Dunia Versi
                      Forbes
                    </CardTitle>
                    <CardSubtitle>19 Apr 2022</CardSubtitle>
                    <CardText
                      style={{
                        color: "#606369",
                        fontSize: "13px",
                        textAlign: "justify",
                      }}
                    >
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </CardText>
                    <Button color="primary">See More</Button>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </Col>
        <Col md="12 mt-5">
          <Container>
            <Row>
              <Col md="6">
                <img
                  src="https://kadetech.in/wp-content/uploads/2019/12/services-image2.png"
                  width="100%"
                />
              </Col>
              <Col md="6">
                <h5 style={{ color: "#2469A5" }}>Mande jadi Digital</h5>
                <p
                  style={{
                    color: "#606369",
                    fontSize: "13px",
                    textAlign: "justify",
                  }}
                >
                  Terus memantapkan diri menjadi yang terdepan dalam arus
                  perkembangan teknologi dan informasi. Bank Mande tak
                  hentinya berbenah untuk maju melalui transformasi, inovasi,
                  keamanan data, dan sumber daya manusia yang unggul. Memberikan
                  makna bagi kehidupan yang lebih baik.
                </p>
              </Col>
            </Row>
          </Container>
        </Col>
      </>
    );
  }
}

export default LandingPage;
