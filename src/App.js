import NavbarComp from "./components/NavbarComp";
import React from "react";
import { Routes, Route, Router } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";
import LandingPage from "./pages/LandingPage";
import FooterComp from "./components/FooterComp";
import RegisterPage from "./pages/RegisterPage";
import TransferPage from "./pages/TransferPage";
import SchedulerPage from "./pages/SchedulerPage";
import UsersPage from "./pages/UsersPage";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container fluid className="p-0">
        <Row>
          <Col md="12">
            <NavbarComp />
            <Routes>
              <Route path="/" caseSensitive={false} element={<LandingPage />} />
              <Route
                path="/register"
                caseSensitive={false}
                element={<RegisterPage />}
              />
              <Route
                path="/transfer"
                caseSensitive={false}
                element={<TransferPage />}
              />
              <Route
                path="/scheduler"
                caseSensitive={false}
                element={<SchedulerPage />}
              />
              <Route
                path="/users"
                caseSensitive={false}
                element={<UsersPage />}
              />
            </Routes>
            <FooterComp />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
